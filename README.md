# Cheap Update Buildpack (proof of concept)

A buildpack that enables to update code of application deployed on Cloud Foundry without redeployment.

It is based on the [SCM Buildpack](https://github.com/jbayer/scm-buildpack) by James Bayer.

ATTENTION: This buildpack is intended to be used for development purpose only. Please do not use it for production deployment, because it breaks some features of Cloud Foundry such as scalability, robustness, high availability and repeatability.

## Motivation

Please refer to https://groups.google.com/a/cloudfoundry.org/forum/#!topic/vcap-dev/grRl-Uh-4Dw

## How It Works

This buildpack launches [a small proxy](https://bitbucket.org/nota-ja/cheap-update-proxy) beside your application process. Usually it proxies accesses to the application. When it receives an access to the special path `/__UPDATE_RESTART`, it first fetches the updated code from the application's git repository, then restarts the process of the application.

## Usage

This buildpack is designed to be used with [heroku-buildpack-multi](https://github.com/ddollar/heroku-buildpack-multi).

- Create a directory that is used as an "application directory" of Cloud Foundry
- Files:
  - Create `.buildpacks` file in the root of the directory. It should have 1 buildpack url per line, the 1st line is likely this cheap-update-buildpack and subsequent lines should be other buildpacks required for your app code to run
  - Create a `.cheap-update-buildpack` file in the root of the directory so the cheap-update-buildpack `bin/detect` script returns a 0 exit code
- Environment variables:
  - Required:
    - Set the `SCM_URL` environment variable with the URL of your git repository of the application to deploy. The URL is assumed to end with `.git`
    - Set the `CU_START` environment variable with an actual start command of your application. If the application is a Web application, you must specify its listening port to `1271`
  - Optional (not yet verified)
    - Set the `SCM_BRANCH` environment variable with the branch name of your application repository you want to use
    - Set the `CU_BEFORE_START` environment variable with a command that should be executed before the application is started, e.g. `bundle install`
- Start command for `cf push`
  - Set the start command of Cloud Foundry deployment to `./cheap-update-proxy`. It is the fixed value used with this buildpack

## Example

- Prepare your application repository  
```
$ git clone https://bitbucket.org/nota-ja/cf-example-sinatra.git
$ cd cf-example-sinatra
# (Create your repo for the application in <YOUR_ORG>)
$ git remote add mine https://github.com/<YOUR_ORG>/cf-example-sinatra.git
$ git push mine master
```
```
$ cat main.rb
require "sinatra"

VERSION = "0.0.1"

get "/" do
  "Hello from ver. #{VERSION}\n"
end
```

- Create directory for application deployment  
```
$ mkdir cub-test
$ cd cub-test
```

- Create files for application deployment  
```
$ cat > .buildpacks <<EOF
https://bitbucket.org/nota-ja/cheap-update-buildpack.git
https://github.com/cloudfoundry/ruby-buildpack.git#c5b2bbe
EOF
```
(*) Unfortunately, the current version ([2477a9b](https://github.com/cloudfoundry/ruby-buildpack/tree/2477a9b0deec72c0972714b485f42906197770f0)) of `cloudfoundry/ruby-buildpack` has and depends on a submodule, so it doesn't work with `ddollar/heroku-buildpack-multi`. You must specify the refspec that points just before the submodule was introduced when you use cloudfoundry/ruby-buildpack.
```
$ touch .cheap-update-buildpack
```

- Create manifest file for application deployment  
```
$ cat > manifest.yml <<EOF
---
applications:
- name: cub-test
  memory: 128M
  buildpack: https://github.com/ddollar/heroku-buildpack-multi.git
  path: .
  command: ./cheap-update-proxy
  env:
    SCM_URL: https://github.com/<YOUR_ORG>/cf-example-sinatra.git
    BEFORE_CU_START: "bundle install --path vendor/bundle"
    CU_START: "bundle exec rackup -o 127.0.0.1 -p 1271"
EOF
```

- Deploy application for the first time  
```
$ cf push
Using manifest file /path/to/cu-test/manifest.yml
...
1 of 1 instances running

App started


OK

App cub-test was started using this command `./cheap-update-proxy`

Showing health and status for app cub-test in org nota / space dev as nota...
OK

requested state: started
instances: 1/1
usage: 128M x 1 instances
urls: cub-test.10.244.0.34.xip.io
package uploaded: Sat Jan 3 00:54:37 +0000 2015

     state     since                    cpu    memory          disk
     #0   running   2015-01-03 09:55:39 AM   0.0%   24.1M of 128M   0 of 1G
```
```
$ curl http://cub-test.10.244.0.34.xip.io/
HTTP/1.1 200 OK
Content-Length: 21
Connection: Keep-Alive
Content-Type: text/html;charset=utf-8
Date: Sat, 03 Jan 2015 03:07:29 GMT
Server: WEBrick/1.3.1 (Ruby/2.0.0/2014-11-13)
X-Content-Type-Options: nosniff
X-Frame-Options: SAMEORIGIN
X-Xss-Protection: 1; mode=block

Hello from ver. 0.0.1
```

- Modify your code  
```
$ cd path/to/cf-example-sinatra/
$ emacs main.rb
..
$ cat main.rb
require "sinatra"

VERSION = "0.0.2"

get "/" do
  "Hello from ver. #{VERSION}\n"
end
$ git diff
diff --git a/main.rb b/main.rb
index fbe2188..00fb576 100644
--- a/main.rb
+++ b/main.rb
@@ -1,6 +1,6 @@
 require "sinatra"

-VERSION = "0.0.1"
+VERSION = "0.0.2"

 get "/" do
   "Hello from ver. #{VERSION}\n"
$ git add -u
$ git commit -m "Update version"
[master 2b8ede0] Update version
 1 file changed, 1 insertion(+), 1 deletion(-)
$ git push mine master
..
```
You may also edit code on GitHub.

- Update application instance on Cloud Foundry  
```
$ curl http://cub-test.10.244.0.34.xip.io/__UPDATE_RESTART
OK
```
```
$ curl http://cub-test.10.244.0.34.xip.io/
HTTP/1.1 200 OK
Content-Length: 21
Connection: Keep-Alive
Content-Type: text/html;charset=utf-8
Date: Sat, 03 Jan 2015 03:10:50 GMT
Server: WEBrick/1.3.1 (Ruby/2.0.0/2014-11-13)
X-Content-Type-Options: nosniff
X-Frame-Options: SAMEORIGIN
X-Xss-Protection: 1; mode=block

Hello from ver. 0.0.2
```
Now you see an updated application response without `cf push`.

## Known Issues

- cheap-update-proxy returns the header of an original response in its response body
- Verified with very limited application/buildpack (only with Sinatra/Ruby)
- Not verified with applications using services
- Not verified with applications without route
- Cannot be used with [the current version](https://github.com/cloudfoundry/ruby-buildpack/tree/2477a9b0deec72c0972714b485f42906197770f0) of `cloudfoundry/ruby-buildpack`  
(In fact, it is not an issue of Cheap Update Buildpack; it is incompatibility between `ddollar/heroku-buildpack-multi` and `cloudfoundry/ruby-buildpack`)
