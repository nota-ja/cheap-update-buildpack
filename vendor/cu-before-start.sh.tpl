#!/bin/bash

set -x # print traces

droplet_base_dir=$(cd $(dirname $0) && cd .. && pwd)

pushd $droplet_base_dir/app
{{.given}}
popd
